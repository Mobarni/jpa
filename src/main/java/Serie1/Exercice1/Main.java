package Serie1.Exercice1;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
        EntityManager em = emf.createEntityManager() ;
        
        System.out.println("OK\n");
        Date d= new Date(18/05/2005);
        Marin m = new Marin();
        m.setName("AOUTA");
        m.setPrenom("Moussa");
        m.setDate_de_naissance(d);
        
        em.getTransaction().begin();
        em.persist(m);
        em.getTransaction().commit();
        
	}

}
