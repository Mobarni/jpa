package Serie1.Exercice1;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity(name= "MARIN")
@Table(
   name="marin",
   uniqueConstraints={
       @UniqueConstraint(name="full_name", columnNames={"family_name", "first_name"})
   }
)
public class Marin implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
   
	 @Column(name="family_name", length=50)
    private String name;
	 @Column(name="first_name", length=50)
    private String prenom;
	 @Column(name="date_de_naissance")
     @Temporal(TemporalType.DATE)
    private Date date_de_naissance;
    
    public Long getId() {
        return id;
   }
    public  void setId(Long id) {
        this.id = id;
   }
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDate_de_naissance() {
		return date_de_naissance;
	}
	public void setDate_de_naissance(Date date_de_naissance) {
		this.date_de_naissance = date_de_naissance;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date_de_naissance == null) ? 0 : date_de_naissance.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (date_de_naissance == null) {
			if (other.date_de_naissance != null)
				return false;
		} else if (!date_de_naissance.equals(other.date_de_naissance))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Marin [id=" + id + ", name=" + name + ", prenom=" + prenom + ", date_de_naissance=" + date_de_naissance
				+ "]";
	}
	
	
  
}
