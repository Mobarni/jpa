package Serie1.Exercice1;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MarinUtil {

	static long create(Marin marin){
		return marin.getId();
	}
	
	static Marin find(long id){
		Marin m= new Marin();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
        EntityManager em = emf.createEntityManager() ;
        
        em.getTransaction().begin();
        m= em.find(Marin.class, id);
        em.getTransaction().commit();
        
		return m;
	}
	static void raiseSalary(int percent){
		
	}
	
	static boolean delete(long id){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
        EntityManager em = emf.createEntityManager() ;
       
        em.getTransaction().begin();
        
        if(em.find(Marin.class, id)==null)
        	return false;
        em.remove(em.find(Marin.class, id));
        em.getTransaction().commit();
        return true;
	}
}
